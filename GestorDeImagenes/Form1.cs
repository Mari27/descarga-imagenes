﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;



namespace GestorDeImagenes
{
    public partial class Form1 : Form
    {
        private string connString;

        public static string ValorGlobal { get; set; } = string.Empty;
        public Form1()
        {

            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        


        private void Form1_Load(object sender, EventArgs e)
        {

            //cboUsu.Items.Add("cesar.zorondo");
            //cboUsu.Items.Add("marcela.baeza");
            cboPrograma.Items.Add("Abbvie");
            cboPrograma.Items.Add("Pfizer");

            cboPrograma.SelectedItem = ValorGlobal;

          

           


        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //if (cboUsu.SelectedIndex < 0)

            //{
            //    MessageBox.Show("Debe completar todos los campos", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            var programa = cboPrograma.SelectedIndex + 1;
            ValorGlobal = cboPrograma.SelectedItem.ToString();
            var nombrePrograma = cboPrograma.SelectedItem.ToString();
            


            

            string user = this.cboUsu.Text;
            string pass = this.textBox1.Text;

            //Cadena de conexion
            
            
            if (ValorGlobal=="Abbvie")
            {
                connString= @"Data Source=3.13.239.186;database=CRM;UID=sa;password=GoPharmaSolutions01";

            }
            else if (ValorGlobal=="Pfizer")
            {
                connString = @"Data Source=3.15.75.89;database=CRM;UID=sa;password=GoPharmaSolutions01";
            }

            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            //Consulta a la base de datos
            

            string query = string.Empty;
            query = "EXEC [general].[PROC_Cuenta_acceso_SELECT_FOR_LOGIN_MMA] '" + cboUsu.Text + "'," + "'" + textBox1.Text + "'";


            SqlCommand command = new SqlCommand(query, conn);

       
            SqlDataReader reader = command.ExecuteReader();


            using (SqlConnection cn = new SqlConnection(connString))
            {
                cn.Open();

                SqlCommand cmd = new SqlCommand(query, cn);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Console.WriteLine(Convert.ToString(dr[0]));

                    Administrador frm1 = new Administrador();
                    frm1.Show();
                    this.Hide();

                }

                else {
                    MessageBox.Show("Usuario o Contraseña invalida", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CboUsu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Btncerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void TextBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "CONTRASEÑA")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
                textBox1.UseSystemPasswordChar = true;
            }
        }

        private void TextBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "CONTRASEÑA";
                textBox1.ForeColor = Color.DimGray;
                textBox1.UseSystemPasswordChar = false;

            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void CboUsu_TextChanged(object sender, EventArgs e)
        {

        }

        private void CboUsu_Enter(object sender, EventArgs e)
        {
            if (cboUsu.Text == "USUARIO")
            {
                cboUsu.Text = "";
                cboUsu.ForeColor = Color.Black;
                //cboUsu.UseSystemPasswordChar = true;
            }
        }

        private void CboUsu_Leave(object sender, EventArgs e)
        {
            if (cboUsu.Text == "")
            {
                cboUsu.Text = "USUARIO";
                cboUsu.ForeColor = Color.DimGray;
                //cboUsu.UseSystemPasswordChar = false;

            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);


        }
    }
}
