﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Drawing;
using GestorDeImagenes.Abbvie;
using GestorDeImagenes.Pfizer;

namespace GestorDeImagenes
{
    public partial class Administrador : Form
    {
        private DataTable dataTable = new DataTable();

        //public string ValorGlobal { get; private set; }

        public Administrador()
        {
            InitializeComponent();

            //pictureBox1.Image = Image.FromFile("logo2.png");

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

    

        private void BtnDescargar_Click(object sender, EventArgs e)
        {
            //validaciones 

            if (cboDocumento.SelectedIndex < 0)

            {
                MessageBox.Show("Debe completar todos los campos", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

          
            


            //if (cboAno.SelectedIndex < 0)

            //{
            //    MessageBox.Show("Debe completar todos los campos", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            //else if (cboMes.SelectedIndex < 0)

            //{
            //    MessageBox.Show("Debe completar todos los campos", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}




            //Crea la opcion para seleccionar la carpeta
            FolderBrowserDialog saveFileDialog1 = new FolderBrowserDialog();

            var ruta = string.Empty;

            int ano;

            var documento = cboDocumento.SelectedIndex + 1;

            int mes;

           

            //var programa = cboPrograma.SelectedIndex + 1;

            //var nombrePrograma = toolStripStatusLabel1.Text;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK) //Abre la wea pa seleccionar la carpeta
            {
                ruta = saveFileDialog1.SelectedPath; //Guarda la ruta seleccionada en la wea
            }
            else
            {
                return; //Al cerrar la opcion no se cae el programa
            }
              splashScreenManager1.ShowWaitForm();
            if (toolStripStatusLabel1.Text == "Abbvie")
            {
                var conexion = new AbbConexion();
                var totalfinal = dataTable.Rows.Count;
               
                //recetas = documento = 1
                if (documento == 1)
                {
                    if (textRut.Text == "")
                    {
                        ano = int.Parse(cboAno.SelectedItem.ToString());
                        mes = cboMes.SelectedIndex + 1;
                        var estadoDescargarReceta = DescargarReceta(conexion.conectar, ano, mes, ruta);//, nombrePrograma
                    }
                    else
                    {
                        var estadoDescargaRecetaRut = DescargarRecetaByRut(conexion.conectar, ruta,textRut.Text);//, nombrePrograma, textRut.Text
                    }
                  
                }

                //consentimiento = documento = 2 
                else if (documento == 2)
                {
                    if (textRut.Text == "")
                    {
                        ano = int.Parse(cboAno.SelectedItem.ToString());
                        mes = cboMes.SelectedIndex + 1;
                        var estadoDescargarConsentimiento = DescargarConsentimiento(conexion.conectar, ano, mes, ruta); //, nombrePrograma
                    }
                    else
                    {
                        var estadoDescargaConsentimientoRut = DescargarConsentimientoByRut(conexion.conectar,ruta,textRut.Text); //, nombrePrograma, textRut.Text
                    }
                   

                }

                //ambos = documento = 3
                //else if (documento == 3)
                //{
                //    if (textRut.Text == "")
                //    {
                //        ano = int.Parse(cboAno.SelectedItem.ToString());
                //        mes = cboMes.SelectedIndex + 1;
                //        var estadoDescargarReceta = DescargarReceta(conexion.conectar, ano, mes, ruta); //, nombrePrograma
                //        var conn = new AbbConexion();
                //        var estadoDescargarConsentimiento = DescargarConsentimiento(conn.conectar, ano, mes, ruta); //, nombrePrograma
                //    }
                //    else
                //    {

                //        var estadoDescargaAmbosRut = DescargarAmbosByRut(conexion.conectar, ruta, textRut.Text); //, nombrePrograma
                //    }
                  

                //}
            }
            else if (toolStripStatusLabel1.Text == "Pfizer")
            {
                var conexion = new PfiConexion();
                var totalfinal = dataTable.Rows.Count;
                
                if (documento == 1)

                {
                    if (textRut.Text == "")
                    {
                        ano = int.Parse(cboAno.SelectedItem.ToString());
                        mes = cboMes.SelectedIndex + 1;
                        var estadoDescargarReceta = DescargarReceta(conexion.conectar, ano, mes, ruta); //, nombrePrograma
                    }
                    else
                    {
                        var estadoDescargaRecetaRut = DescargarRecetaByRut(conexion.conectar, ruta, textRut.Text); //, nombrePrograma
                    }

                  
                }


                else if (documento == 2)
                {
                    if (textRut.Text == "")
                    {
                        ano = int.Parse(cboAno.SelectedItem.ToString());
                        mes = cboMes.SelectedIndex + 1;
                        var estadoDescargarConsentimiento = DescargarConsentimiento(conexion.conectar, ano, mes, ruta); //, nombrePrograma
                    }
                    else
                    {
                        var estadoDescargaConsentimientoRut = DescargarConsentimientoByRut(conexion.conectar, ruta, textRut.Text); //, nombrePrograma
                    }
                 
                }
             
                //else if (documento == 3)
                //{
                //    if (textRut.Text == "")
                //    {
                //        ano = int.Parse(cboAno.SelectedItem.ToString());
                //        mes = cboMes.SelectedIndex + 1;
                //        var estadoDescargarReceta = DescargarReceta(conexion.conectar, ano, mes, ruta); //, nombrePrograma
                //        var conn = new AbbConexion();
                //        var estadoDescargarConsentimiento = DescargarConsentimiento(conn.conectar, ano, mes, ruta); //, nombrePrograma
                //    }
                //    else
                //    {
                //        var estadoDescargaAmbosRut = DescargarAmbosByRut(conexion.conectar, ruta, textRut.Text); //, nombrePrograma
                //    }
                    

                //}


            }
            else
            {
                return;
            }
            splashScreenManager1.CloseWaitForm();
            MessageBox.Show("Proceso terminado","Informacion",MessageBoxButtons.OK,MessageBoxIcon.Information);
            

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void CboAno_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CboPrograma_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Administrador_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = Form1.ValorGlobal;

          
            #region Inicializacion de componentes
            //cboPrograma.Items.Add("Abbvie");
            //cboPrograma.Items.Add("Pfizer");
            cboDocumento.Items.Add("Receta");
            cboDocumento.Items.Add("Consentimiento");
            //cboDocumento.Items.Add("Ambos");
            cboMes.Items.Add("Enero");
            cboMes.Items.Add("Febrero");
            cboMes.Items.Add("Marzo");
            cboMes.Items.Add("Abril");
            cboMes.Items.Add("Mayo");
            cboMes.Items.Add("Junio");
            cboMes.Items.Add("Julio");
            cboMes.Items.Add("Agosto");
            cboMes.Items.Add("Septiembre");
            cboMes.Items.Add("Octubre");
            cboMes.Items.Add("Noviembre");
            cboMes.Items.Add("Diciembre");

            DateTime fechaActual = DateTime.Now;
            for (int i = 2016; i <= fechaActual.Year; i++)
            {
                this.cboAno.Items.Add(i);
            }
            #endregion
        }

        private void CboDocumento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //Metodo de descarga imagenes
        public bool DescargarReceta(SqlConnection conexion, int ano, int mes, string ruta)//, string nombrePrograma
        {
            try
            {
                using (SqlConnection con = conexion)
                {
                    using (SqlCommand cmd = new SqlCommand("[general].[PROC_GET_Receta_By_Ano_Mes]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ano", SqlDbType.Int).Value = ano;
                        cmd.Parameters.Add("@mes", SqlDbType.Int).Value = mes;

                        con.Open();
                        cmd.CommandTimeout = 1200;  //ESTABLECE EL TIEMPO DE ESPERA 1200= 20 min
                        cmd.ExecuteNonQuery();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);



                        da.Fill(dataTable);

                       

                        //Variable que se ocupa despues
                        Image rImage = null;
                        var totalfinal = dataTable.Rows.Count;
                        var incremento = 1;
                        progressBar1.Value = 0;
                        progressBar1.Maximum = totalfinal;

                        if (totalfinal == 0)
                        {
                            MessageBox.Show("NO SE ENCONTRARON REGISTROS","ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }


                        //Recorre el datatable (es donde esta los datos de la consulta SQL)
                        foreach (DataRow dtRow in dataTable.Rows)
                        {

                            var TratamientoId = dtRow["id"].ToString();

                            var Reportaccion = dtRow["report_accion"].ToString();

                            var pacienteId = dtRow["Paciente_id"].ToString();

                            var fechaCierre = dtRow["fecha_cierre"].ToString();

                            var productoId = dtRow["Producto_id"].ToString();

                            var consentimiento_fecha = dtRow["receta_fecha"].ToString();

                            if (!String.IsNullOrEmpty(fechaCierre))
                            {
                                DateTime? FechaIniF = dtRow["fecha_cierre"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dtRow["fecha_cierre"]);
                                if (FechaIniF.HasValue)
                                {
                                    var trataprueba = dtRow["id"].ToString();
                                    var receta = (byte[])dtRow["receta"];

                                    DateTime DtFechaCierre = DateTime.Parse(fechaCierre);
                                    string carpeta = DtFechaCierre.Year.ToString() + "_" + DtFechaCierre.Month.ToString();
                                    var appDomain = AppDomain.CurrentDomain;

                                    string path = string.Format("{0}\\{1}\\RECETAS\\{2}_{3}", ruta,toolStripStatusLabel1.Text,DtFechaCierre.Year, DtFechaCierre.Month); 

                                if (!Directory.Exists(path))
                                    {
                                        DirectoryInfo di = Directory.CreateDirectory(path);
                                    }

                                    using (MemoryStream ms = new MemoryStream(receta))
                                    {
                                        using (rImage = Image.FromStream(ms))
                                        {
                                            rImage.Save(path + string.Format("\\REC_{0}_{1}.JPG", pacienteId, productoId), System.Drawing.Imaging.ImageFormat.Jpeg);
                                        }

                                    }
                                }
                        
                            }
                            //splashScreenManager1.CloseWaitForm();
                            progressBar1.Value = progressBar1.Value + incremento;
                          
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool DescargarConsentimiento(SqlConnection conexion, int ano, int mes, string rutaa)//, string nombreProgram
        {
            //try
            //{
                using (SqlConnection con = conexion)
                {
                    using (SqlCommand cmd = new SqlCommand("[general].[PROC_GET_Consentimiento_By_Ano_Mes]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ano", SqlDbType.Int).Value = ano;
                        cmd.Parameters.Add("@mes", SqlDbType.Int).Value = mes;

                        con.Open();
                        cmd.CommandTimeout = 1200;
                        cmd.ExecuteNonQuery();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);

                        da.Fill(dataTable);
                      

                        //Variable que se ocupa despues
                        Image rImage = null;
                        var totalfinal = dataTable.Rows.Count;
                        var incremento = 1;
                        progressBar1.Value = 0;
                        progressBar1.Maximum = totalfinal;

                        if (totalfinal == 0)
                        {
                            MessageBox.Show("NO SE ENCONTRARON REGISTROS","ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }

                        //Recorre el datatable (es donde esta los datos de la consulta SQL)
                        foreach (DataRow dtRow in dataTable.Rows)
                        {
                            //progressBarControl1.EditValue =
                            var TratamientoId = dtRow["id"].ToString();

                            var Reportaccion = dtRow["report_accion"].ToString();

                            var pacienteId = dtRow["Paciente_id"].ToString();

                            var consentimientoFecha = dtRow["fecha_cierre_con"].ToString();

                            var productoId = dtRow["Producto_id"].ToString();

                            //var consentimiento_fecha = dtRow["consentimiento_fecha"].ToString();
                            
                          

                            if (!String.IsNullOrEmpty(consentimientoFecha))
                            {
                                DateTime? FechaIniF = dtRow["fecha_cierre_con"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dtRow["fecha_cierre_con"]);
                                if (FechaIniF.HasValue)
                                {
                                    var trataprueba = dtRow["id"].ToString();
                                    var consentimiento = (byte[])dtRow["consentimiento"];

                                    DateTime DtConsentimientoFecha = DateTime.Parse(consentimientoFecha);
                                    string carpeta = DtConsentimientoFecha.Year.ToString() + "_" + DtConsentimientoFecha.Month.ToString();
                                    var appDomain = AppDomain.CurrentDomain;

                                    string path = string.Format("{0}\\{1}\\CONSENTIMIENTOS\\{2}_{3}", rutaa, toolStripStatusLabel1.Text, DtConsentimientoFecha.Year, DtConsentimientoFecha.Month); 

                                    if (!Directory.Exists(path))
                                    {
                                        DirectoryInfo di = Directory.CreateDirectory(path);
                                    }

                                    using (MemoryStream ms = new MemoryStream(consentimiento))
                                    {
                                        using (rImage = Image.FromStream(ms))
                                        {
                                            rImage.Save(path + string.Format("\\CONS_{0}_{1}.JPG", pacienteId, productoId), System.Drawing.Imaging.ImageFormat.Jpeg);
                                        }

                                    }
                                }
                            }
                            progressBar1.Value = progressBar1.Value + incremento;
                        }
                    }
                }
                return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}

        }

        public bool DescargarRecetaByRut(SqlConnection conexion, string ruta, string rut) //, string nombrePrograma
        {
          //  try
          //  {
                using (SqlConnection con = conexion)
                {
                    using (SqlCommand cmd = new SqlCommand("[general].[PROC_RECETA_and_CONSENTIMIENTO_SELECT_BY_RUT]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@rut_paciente", SqlDbType.VarChar).Value = rut;
                        cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = 1;
                        


                        con.Open();
                        cmd.CommandTimeout = 1200;
                        cmd.ExecuteNonQuery();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);



                        da.Fill(dataTable);

                       

                        //Variable que se ocupa despues
                        Image rImage = null;
                        var totalfinal = dataTable.Rows.Count;
                        var incremento = 1;
                        progressBar1.Value = 0;
                        progressBar1.Maximum = totalfinal;

                        if (totalfinal == 0)
                        {
                            MessageBox.Show("NO SE ENCONTRARON REGISTROS","ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }



                        //Recorre el datatable (es donde esta los datos de la consulta SQL)
                        foreach (DataRow dtRow in dataTable.Rows)
                        {
                        
                            var pacienteId = dtRow["Paciente_id"].ToString();

                            //var recetaFecha = dtRow["receta_fecha"].ToString();

                            var productoId = dtRow["Producto_id"].ToString();

                            var fechaCierre = dtRow["fecha_cierre"].ToString();

                           
                            if (!String.IsNullOrEmpty(fechaCierre))
                            {
                                DateTime? FechaIniF = dtRow["fecha_cierre"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dtRow["fecha_cierre"]);
                                if (FechaIniF.HasValue)
                                {
                                    var trataprueba = dtRow["Paciente_id"].ToString();
                                    var receta = (byte[])dtRow["receta"];

                                    DateTime DtRecetaFecha = DateTime.Parse(fechaCierre);
                                    string carpeta = DtRecetaFecha.Year.ToString() + "_" + DtRecetaFecha.Month.ToString();
                                    var appDomain = AppDomain.CurrentDomain;

                                    string path = string.Format("{0}\\{1}\\RECETAS\\{2}_{3}", ruta,toolStripStatusLabel1.Text, DtRecetaFecha.Year, DtRecetaFecha.Month); 

                                    if (!Directory.Exists(path))
                                    {
                                        DirectoryInfo di = Directory.CreateDirectory(path);
                                    }

                                    using (MemoryStream ms = new MemoryStream(receta))
                                    {
                                        using (rImage = Image.FromStream(ms))
                                        {
                                            rImage.Save(path + string.Format("\\REC_{0}_{1}.JPG", pacienteId, productoId), System.Drawing.Imaging.ImageFormat.Jpeg);
                                        }

                                    }
                                }
                            }
                            
                            progressBar1.Value = progressBar1.Value + incremento;
                            
                        }
                    }
                }
               
                return true;
            //}
           // catch (Exception ex)
           // {
           //     return false;
          //  }
        }

        private bool DescargarConsentimientoByRut(SqlConnection conexion, string ruta, string rut)//, string nombrePrograma
        {
          //  try
          //  {
                using (SqlConnection con = conexion)
                {
                    using (SqlCommand cmd = new SqlCommand("[general].[PROC_RECETA_and_CONSENTIMIENTO_SELECT_BY_RUT]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@rut_paciente", SqlDbType.VarChar).Value = rut;
                        cmd.Parameters.Add("@tipo", SqlDbType.Int).Value = 2;


                    con.Open();
                        cmd.CommandTimeout = 1200;  //ESTABLECE EL TIEMPO DE ESPERA 1200= 20 min
                        cmd.ExecuteNonQuery();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);



                        da.Fill(dataTable);

                       

                        //Variable que se ocupa despues
                        Image rImage = null;
                        var totalfinal = dataTable.Rows.Count;
                        var incremento = 1;
                        progressBar1.Value = 0;
                        progressBar1.Maximum = totalfinal;

                        if (totalfinal == 0)
                        {
                            MessageBox.Show("NO SE ENCONTRARON REGISTROS","ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }




                        //Recorre el datatable (es donde esta los datos de la consulta SQL)
                        foreach (DataRow dtRow in dataTable.Rows)
                        {

                            

                            var pacienteId = dtRow["Paciente_id"].ToString();

                            //var recetaFecha = dtRow["consentimiento_fecha"].ToString();

                            var productoId = dtRow["Producto_id"].ToString();

                            //var consentimiento_fecha = dtRow["consentimiento_fecha"].ToString();

                            var fechaCierre = dtRow["fecha_cierre_con"].ToString();



                            if (!String.IsNullOrEmpty(fechaCierre))
                            {
                                DateTime? FechaIniF = dtRow["fecha_cierre_con"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dtRow["fecha_cierre_con"]);
                                if (FechaIniF.HasValue)
                                {
                                    var trataprueba = dtRow["Paciente_id"].ToString();
                                    var consentimiento = (byte[])dtRow["consentimiento"];

                                    DateTime DtConsentimientoFecha = DateTime.Parse(fechaCierre);
                                    string carpeta = DtConsentimientoFecha.Year.ToString() + "_" + DtConsentimientoFecha.Month.ToString();
                                    var appDomain = AppDomain.CurrentDomain;

                                    string path = string.Format("{0}\\{1}\\ CONSENTIMIENTOS\\{2}_{3}", ruta, toolStripStatusLabel1.Text, DtConsentimientoFecha.Year, DtConsentimientoFecha.Month);

                                    if (!Directory.Exists(path))
                                    {
                                        DirectoryInfo di = Directory.CreateDirectory(path);
                                    }

                                    using (MemoryStream ms = new MemoryStream(consentimiento))
                                    {
                                        using (rImage = Image.FromStream(ms))
                                        {
                                            rImage.Save(path + string.Format("\\CONS_{0}_{1}.JPG", pacienteId, productoId), System.Drawing.Imaging.ImageFormat.Jpeg);
                                        }

                                    }
                                }
                            }
                     
                            progressBar1.Value = progressBar1.Value + incremento;
                            
                        }
                    }
                }
                return true;
           // }
            //catch (Exception ex)
           // {
            //    return false;
           // }
        }

        //private bool DescargarAmbosByRut(SqlConnection conexion, string ruta, string rut) //, string nombrePrograma
        //{
        //    try
        //    {
        //        using (SqlConnection con = conexion)
        //        {
        //            using (SqlCommand cmd = new SqlCommand("[papa].[PROC_RECETA_and_CONSENTIMIENTO_SELECT_BY_RUT]", con))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.Add("@rut_paciente", SqlDbType.VarChar).Value = rut;


        //                con.Open();
        //                cmd.ExecuteNonQuery();
        //                SqlDataAdapter da = new SqlDataAdapter(cmd);



        //                da.Fill(dataTable);

        //                //splashScreenManager1.CloseWaitForm();

        //                //Variable que se ocupa despues
        //                Image rImage = null;
        //                var totalfinal = dataTable.Rows.Count;
        //                var incremento = 1;
        //                progressBar1.Value = 0;
        //                progressBar1.Maximum = totalfinal;

        //                if (totalfinal == 0)
        //                {
        //                    MessageBox.Show("NO SE ENCONTRARON REGISTROS","ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                    return false;
        //                }




        //                //Recorre el datatable (es donde esta los datos de la consulta SQL)
        //                foreach (DataRow dtRow in dataTable.Rows)
        //                {

        //                    //var TratamientoId = dtRow["id"].ToString();

        //                    var pacienteId = dtRow["Paciente_id"].ToString();

        //                    var productoId = dtRow["Producto_id"].ToString();

        //                    var consentimiento_fecha = dtRow["consentimiento_fecha"].ToString();

        //                    var receta_fecha = dtRow["receta_fecha"].ToString();

        //                    if (!String.IsNullOrEmpty(consentimiento_fecha))
        //                    {
        //                        DateTime? FechaIniF = dtRow["consentimiento_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dtRow["consentimiento_fecha"]);
        //                        if (FechaIniF.HasValue)
        //                        {
        //                            var trataprueba = dtRow["paciente_id"].ToString();
        //                            var consentimiento = (byte[])dtRow["consentimiento_imagen"];

        //                            DateTime DtConsentimientoFecha = DateTime.Parse(consentimiento_fecha);
        //                            string carpeta = DtConsentimientoFecha.Year.ToString() + "_" + DtConsentimientoFecha.Month.ToString();
        //                            var appDomain = AppDomain.CurrentDomain;

        //                            string path = string.Format("{0}\\{1}\\ CONSENTIMIENTOS\\{2}_{3}", ruta, DtConsentimientoFecha.Year, DtConsentimientoFecha.Month); //, nombrePrograma

        //                            if (!Directory.Exists(path))
        //                            {
        //                                DirectoryInfo di = Directory.CreateDirectory(path);
        //                            }

        //                            using (MemoryStream ms = new MemoryStream(consentimiento))
        //                            {
        //                                using (rImage = Image.FromStream(ms))
        //                                {
        //                                    rImage.Save(path + string.Format("\\CONS_{0}_{1}.JPG", pacienteId, productoId), System.Drawing.Imaging.ImageFormat.Jpeg);
        //                                }

        //                            }
        //                        }
        //                    }

        //                    if (!String.IsNullOrEmpty(receta_fecha))
        //                    {
        //                        DateTime? FechaIniF = dtRow["receta_fecha"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dtRow["receta_fecha"]);
        //                        if (FechaIniF.HasValue)
        //                        {
        //                            //var trataprueba = dtRow["id"].ToString();
        //                            var receta = (byte[])dtRow["receta_imagen"];

        //                            DateTime DtRecetaFecha = DateTime.Parse(receta_fecha);
        //                            string carpeta = DtRecetaFecha.Year.ToString() + "_" + DtRecetaFecha.Month.ToString();
        //                            var appDomain = AppDomain.CurrentDomain;

        //                            string path = string.Format("{0}\\{1}\\RECETAS\\{2}_{3}", ruta, DtRecetaFecha.Year, DtRecetaFecha.Month); //, nombrePrograma

        //                            if (!Directory.Exists(path))
        //                            {
        //                                DirectoryInfo di = Directory.CreateDirectory(path);
        //                            }

        //                            using (MemoryStream ms = new MemoryStream(receta))
        //                            {
        //                                using (rImage = Image.FromStream(ms))
        //                                {
        //                                    rImage.Save(path + string.Format("\\REC_{0}_{1}.JPG", pacienteId, productoId), System.Drawing.Imaging.ImageFormat.Jpeg);
        //                                }

        //                            }
        //                        }
        //                    }
        //                    //splashScreenManager1.CloseWaitForm();
        //                    progressBar1.Value = progressBar1.Value + incremento;
                           
        //                }
        //            }
        //        }
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}




        private void ProgressPanel1_Click(object sender, EventArgs e)
        {
            
        }


        private void ProgressBarControl1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {


        }

        private void StatusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void TabPage1_Click(object sender, EventArgs e)
        {

        }

        private void Panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void ToolStripLabel1_Click(object sender, EventArgs e)
        {
   
        }

        private void MenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void AbbvieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 Adm = new Form1();
            Form1.ValorGlobal = "Abbvie";
            Adm.Show();
            this.Hide();
            


        }

        private void PfizerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 Adm = new Form1();
            Form1.ValorGlobal = "Pfizer";
            Adm.Show();
            this.Hide();
           
        }

        private void CambiarProgramaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ToolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void TabPage2_Click(object sender, EventArgs e)
        {
           

        }

        private void TabPage1_Click_1(object sender, EventArgs e)
        {

        }

        private void Panel3_Paint_1(object sender, PaintEventArgs e)
        {
           
        }

        private void CboAno_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            

             
        }
    }
}

